# README 

# Project 
A simple calculator for basic conversion between units developed with Java and html.It covers feet, meters, celsius, and
fahrenheit conversions.

# License 
This project is under MIT license.

# Requirements 
Visual Studio Code.

# Download 
Download the project from the download section or clone it this project by typing 
in the bash the following command:
git clone https://Afsheen_Amir@bitbucket.org/afsheen_amir/assignment03.git

# Running the Application:
Run this project, open the project folder in a webserver, then run the html file.

# Contribution:
If you want to contribute to this project and make it better with new ideas, your 
pull request is very welcome.If you find any issues just put it in the 
repository issue section,
thank You.

# Developer: Afsheen Amir


